FROM python:3.7-stretch
ENV PYTHONBUFFERED 1

RUN mkdir /downloads
WORKDIR /downloads

RUN apt-get update && apt-get install apt-transport-https gcc unixodbc-dev libpq-dev libaio1 libaio-dev alien -y

RUN wget https://packages.microsoft.com/debian/9/prod/pool/main/m/msodbcsql17/msodbcsql17_17.3.1.1-1_amd64.deb
RUN ACCEPT_EULA=Y apt install ./msodbcsql17_17.3.1.1-1_amd64.deb -y

RUN rm -rf /downloads/msodbcsql17_17.3.1.1-1_amd64.deb

RUN wget https://dev.mysql.com/get/Downloads/Connector-ODBC/5.3/mysql-connector-odbc-5.3.10-linux-ubuntu16.04-x86-64bit.tar.gz
RUN tar -xvf mysql-connector-odbc-5.3.10-linux-ubuntu16.04-x86-64bit.tar.gz
#RUN mkdir /usr/lib/x86_64-linux-gnu/odbc/
RUN cp mysql-connector-odbc-5.3.10-linux-ubuntu16.04-x86-64bit/lib/libmyodbc5* /usr/lib/x86_64-linux-gnu/odbc/
RUN /downloads/mysql-connector-odbc-5.3.10-linux-ubuntu16.04-x86-64bit/bin/myodbc-installer -d -a -n "MySQL" -t "DRIVER=/usr/lib/x86_64-linux-gnu/odbc/libmyodbc5w.so;"

RUN rm -rf /downloads/mysql-connector-odbc-5.3.10-linux-ubuntu16.04-x86-64bit.tar.gz
RUN rm -rf /downloads/mysql-connector-odbc-5.3.10-linux-ubuntu16.04-x86-64bit

RUN wget https://download.oracle.com/otn_software/linux/instantclient/195000/oracle-instantclient19.5-basic-19.5.0.0.0-1.x86_64.rpm
RUN wget https://download.oracle.com/otn_software/linux/instantclient/195000/oracle-instantclient19.5-odbc-19.5.0.0.0-1.x86_64.rpm
RUN alien -i oracle-instantclient19.5-basic-19.5.0.0.0-1.x86_64.rpm
RUN alien -i oracle-instantclient19.5-odbc-19.5.0.0.0-1.x86_64.rpm

RUN rm -rf /downloads/oracle-instantclient19.5-basic-19.5.0.0.0-1.x86_64.rpm
RUN rm -rf /downloads/oracle-instantclient19.5-odbc-19.5.0.0.0-1.x86_64.rpm

ADD odbcinst.ini /etc/odbcinst.ini

ENV ORACLE_HOME /usr/lib/oracle/19.5/client64
ENV LD_LIBRARY_PATH /usr/lib/oracle/19.5/client64/lib
ENV ORACLE_SID XE

RUN mkdir /code
WORKDIR /code

ADD requirements.txt /code/
RUN pip install -r requirements.txt

CMD ["python", "manage.py", "runserver","0.0.0.0:8000"]