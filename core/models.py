# -*- coding: UTF-8 -*-
from django.db import models
from core import constants as cons
from core.utils import uuid_str


class Client(models.Model):
    key = models.CharField(primary_key=True, max_length=64, default=uuid_str, editable=False)
    name = models.CharField(max_length=255, verbose_name='Nome')
    client_slug = models.SlugField(blank=True, verbose_name='Site')
    connection_string = models.CharField(max_length=300, null=True)
    token = models.CharField(max_length=40, verbose_name='Token')
    
    def __str__(self):
        return self.name


class BaseClient(models.Model):
    class Meta:
        abstract = True

    client = models.ForeignKey(Client, on_delete=models.CASCADE, verbose_name='Cliente')


class BaseModel(BaseClient):
    class Meta:
        abstract = True

    last_update = models.DateTimeField(blank=True, null=True, auto_now=True)
    creation_date = models.DateTimeField(blank=True, null=True, auto_now_add=True)


class Service(BaseModel):

    class Meta:
        ordering = ('order',)

    def __str__(self):
        return self.name

    order = models.IntegerField()
    name = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)


class WebConfig(BaseModel):
    sgp_url = models.URLField(max_length=300)
    sgp_path_to_list = models.CharField(max_length=100)
    sgp_request_body = models.TextField()

    service_url = models.URLField(max_length=300)
    service_request_body = models.TextField()

    service = models.ForeignKey(Service, null=True, on_delete=models.CASCADE)

    class Meta:
        ordering = ('service__order',)

    def __str__(self):
        return self.service.name


class QueryConfig(BaseModel):
    SEND_TO_SGP = "SEND"
    RECEIVE_FROM_SGP = "RECEIVE"
    MODE = (
        (SEND_TO_SGP, 'Envia para SGP'),
        (RECEIVE_FROM_SGP, 'Recebe do SGP')
    )

    query = models.TextField()
    service_url = models.CharField(max_length=300, null=True)
    mode = models.CharField(max_length=10, choices=MODE, default=SEND_TO_SGP)
    update_data = models.BooleanField(default=False)
    update_status = models.BooleanField(default=False)
    reuse_key_if_duplicated = models.BooleanField(default=False)

    service = models.ForeignKey(Service, null=True, on_delete=models.CASCADE)

    class Meta:
        ordering = ['service__order']

    def __str__(self):
        return self.service.name

    @property
    def send_to_sgp(self):
        return self.mode == self.SEND_TO_SGP

    @property
    def receive_from_sgp(self):
        return self.mode == self.RECEIVE_FROM_SGP


class SyncControl(BaseClient):
    start = models.DateTimeField(auto_now_add=True, verbose_name="Data de inicio")
    end = models.DateTimeField(null=True, verbose_name="Data de fim")
    queries_config = models.ManyToManyField(QueryConfig)

    def __str__(self):
        return '{} - {}'.format(self.start, self.end)


class WebSyncControl(BaseClient):
    start = models.DateTimeField(auto_now_add=True, verbose_name="Data de inicio")
    end = models.DateTimeField(null=True, verbose_name="Data de fim")
    web_configs = models.ManyToManyField(WebConfig)

    def __str__(self):
        return '{} - {}'.format(self.start, self.end)


class SyncLog(BaseClient):

    INFO = 'I'
    WARNING = 'W'
    ERROR = 'E'

    TYPES = (
        (INFO, 'Informação'),
        (WARNING, 'Alerta'),
        (ERROR,  'Erro')
    )

    sync = models.ForeignKey(SyncControl, on_delete=models.CASCADE)
    query_config = models.ForeignKey(QueryConfig, on_delete=models.CASCADE)
    type = models.CharField(choices=TYPES, max_length=1)
    message = models.TextField()

    def __str__(self):
        type = dict(self.TYPES).get(self.type)
        return '{} - {} - {}'.format(self.sync, type, self.message)


class WebSyncLog(BaseClient):

    INFO = 'I'
    WARNING = 'W'
    ERROR = 'E'

    TYPES = (
        (INFO, 'Informação'),
        (WARNING, 'Alerta'),
        (ERROR,  'Erro')
    )

    sync = models.ForeignKey(WebSyncControl, on_delete=models.CASCADE)
    web_config = models.ForeignKey(WebConfig, on_delete=models.CASCADE)
    type = models.CharField(choices=TYPES, max_length=1)
    message = models.TextField()

    def __str__(self):
        type = dict(self.TYPES).get(self.type)
        return '{} - {} - {}'.format(self.sync, type, self.message)


class SyncData(BaseModel):
    legacy_key = models.CharField(max_length=100, null=True)
    sgp_key = models.CharField(max_length=100, null=True)
    name = models.CharField(max_length=200, null=True)
    query_config = models.ForeignKey(QueryConfig, on_delete=models.CASCADE, null=True)
    last_update = models.DateTimeField(blank=True, null=True, auto_now=True)
    last_sync_control = models.ForeignKey(SyncControl, on_delete=models.CASCADE, null=True)
    is_data_active = models.BooleanField(default=True)

    def __str__(self):
        return '{} - {}'.format(self.query_config.service.name, self.name)

    def activate_sync_data(self):
        self.is_data_active = cons.EnumStatus.active
        self.save()

    def inactivate_sync_data(self):
        self.is_data_active = cons.EnumStatus.inactive
        self.save()

    class Meta:
        unique_together = (('legacy_key', 'query_config'), ('sgp_key', 'query_config'),)


class SSOToken(BaseModel):
    username = models.CharField(max_length=100, null=True)
    token = models.CharField(max_length=100, null=True)
    module_login = models.CharField(max_length=100, null=True)
    expiration_date = models.DateTimeField(blank=True, null=True, auto_now=True)

    def __str__(self):
        return '{} - {}'.format(self.username, self.token)
