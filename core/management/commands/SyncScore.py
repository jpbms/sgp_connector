import json
import sys
from datetime import date, timedelta

import requests
from django.core.management import BaseCommand

from sgp_plugin_totvs.models import SyncData


class Command(BaseCommand):
    def get_from_sgp(self):
        # reference_date = date.today() - timedelta(1)
        reference_date = date.today() - timedelta(20)
        # url = "http://conceito-sgp.starlinetecnologia.com.br/csa/api/v1/s/rest/schedulecandidatexplace/" \
        #       "?status_item_generated_id=9&last_update={}".format(reference_date.strftime('%Y-%m-%d'))
        url = "http://localhost:8000/csa/api/v1/s/rest/schedulecandidatexplace/" \
              "?status_item_generated_id=9&last_update={}".format(reference_date.strftime('%Y-%m-%d'))
        headers = {
            'Authorization': 'Token 571b8bf5a6418167bc5205760dd4171d29dba4ab',
            'Content-type': 'application/json'
        }

        response = requests.get(url, headers=headers)

        response_data = response.json()

    #     response_data = json.loads("""[
    # {
    #     "key": "2793a972-a941-11e7-bf8a-0242ac110013",
    #     "place": "93996a9a-91e4-11e7-b8a5-0242ac110008",
    #     "schedule": "79dbb326-910f-11e8-aef1-0242ac11001c",
    #     "candidate": "e21acce4-90fe-11e8-aef1-0242ac11001c",
    #     "score": "6,00",
    #     "status_item_generated": "9",
    #     "attendance": true
    # }]""")

        return response_data

    def send_to_tovs(self, xml):
        url = "https://csasictestes.santoagostinho.com.br:8051/wsDataServer/IwsDataServer"
        headers = {
            'Content-type': 'text/xml;charset=\"UTF-8\"',
            'SOAPAction': 'http://www.totvs.com/IwsDataServer/SaveRecord'
        }
        print(xml)
        response = requests.post(url, data=xml, headers=headers, auth=('starline', 'starline2018'), verify=False)

        print(response)

    def format_data(self, data):

        # schedule_key = SyncData.objects.get(sgp_key=data["schedule"], query_config__name="Schedule_Santo_Agostinho").client_key
        candidate_key = SyncData.objects.get(sgp_key=data["candidate"], query_config__name="Candidate_Santo_Agostinho").client_key
        score = data["score"]
        # schedule_array = schedule_key.split('_')
        # codprova = schedule_array[1]
        # codetapa = schedule_array[2]
        # idturmadisc = schedule_array[3]
        # schedule_array = schedule_key.split('_')
        codprova = 4
        codetapa = 7
        idturmadisc = 24782  # schedule_array[3]


        xml = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tot="http://www.totvs.com/">
    <soapenv:Header/>
    <soapenv:Body>
        <tot:SaveRecord>
            <!--Optional:-->
            <tot:DataServerName>EduAlunoProvaData</tot:DataServerName>
            <!--Optional:-->
            <tot:XML><![CDATA[<EduNotas>
  <SNotas>
    <CODCOLIGADA>1</CODCOLIGADA>
    <CODPROVA>{}</CODPROVA>
    <CODETAPA>{}</CODETAPA>
    <TIPOETAPA>N</TIPOETAPA>
    <IDTURMADISC>{}</IDTURMADISC>
    <RA>{}</RA>
    <NOTA>{}</NOTA>
  </SNotas>
</EduNotas>]]></tot:XML>
            <!--Optional:-->
            <tot:Contexto>CODCOLIGADA=1;CODFILIAL=13;CODTIPOCURSO=3;CODSISTEMA=S</tot:Contexto>
        </tot:SaveRecord>
    </soapenv:Body>
</soapenv:Envelope>""".format(codprova, codetapa, idturmadisc, candidate_key, str(score).replace('.', ','))

        return xml

    def handle(self, *args, **options):
        sxclist = self.get_from_sgp()

        for sxc in sxclist:
            try:
                data = self.format_data(sxc)
                self.send_to_tovs(data)
            except:
                print('Erro ao enviar dado: ', sys.exc_info()[0])
