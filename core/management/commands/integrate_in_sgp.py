from django.core.management import BaseCommand, CommandError

from core import services, models


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('client_slug', nargs='+', type=str)

    def handle(self, *args, **options):
        try:
            client_slug = options['client_slug'][0]
            client = models.Client.objects.get(client_slug=client_slug)
        except models.Client.DoesNotExist:
            raise CommandError('Client %s does not exist' % client_slug)

        integration = services.IntegrationService(services.ReportStatusConsole(), client)
        integration.execute()
