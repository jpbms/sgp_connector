#!/usr/bin/python
# -*- encoding: utf-8 -*-
import logging
import re
from enum import Enum

from core import models, objects, constants
from core.exceptions import NotSyncedForTransException

LOG = logging.getLogger(constants.DEFAULT_LOGGER)


class RowAdapter(object):

    def convert_row_to_object(self, row):
        return self.convert(row)

    def convert(self, row):
        data = {}

        descriptions = row.cursor_description

        for index, description in enumerate(descriptions):
            value = row[index]
            attribute = description[0]

            data = self.process_attribute(data, attribute, value)

        return data

    def process_attribute(self, data, attribute, value):
        if not value:
            return data

        if isinstance(value, str):
            value = value.strip()

        splited = attribute.split('.')
        tmp_data = data

        current_property = None
        for index, tmpattribute in enumerate(splited):
            current_property = objects.AttributeProperty()
            current_property.parse(tmpattribute)

            if current_property.name not in tmp_data:
                tmp_data[current_property.name] = {}

            if index < len(splited) - 1:
                tmp_data = tmp_data[current_property.name]

        if current_property.isarray:
            tmp_data[current_property.name] = []
            for current_value in str(value).split('|'):
                tmp_data[current_property.name].append(self.process_value(current_property.function, current_value))
        else:
            tmp_data[current_property.name] = self.process_value(current_property.function, value)

        return data

    def process_value(self, function, value):
        if (not function) or (not value):
            return value

        result = re.search(r'(?P<function>.+)\((?P<parameter>.*)\)', function)
        if not result:
            return value

        function_name = result.group('function')
        parameter = result.group('parameter')
        if function_name == 'int':
            value = int(value)
        elif function_name == 'str':
            value = str(value)
        elif function_name == 'bool':
            value = bool(value)
        elif function_name == 'trans':
            service_name = parameter
            if value:
                sync_data = models.SyncData.objects.filter(legacy_key=value, query_config__service__name=service_name)
                if sync_data.exists():
                    value = sync_data[0].sgp_key
                else:
                    message = 'O objeto {} de valor {} não foi sincronizado anteriormente.'.format(parameter, value)
                    LOG.warning(
                        msg=message
                    )
                    raise Exception(message)

        return value


class TextTradutor(object):

    @staticmethod
    def _replace_query_plaholder(text, data):
        params = re.findall(r"(?<=\$\{).+?(?=\})", text)
        for param in params:
            value = data.get(param)
            if value is None:
                value = 'Null'
            else:
                value = str(value)
            text = text.replace('${'+param+'}', value)

        return text

    def convert(self, text, data):
        return self._replace_query_plaholder(text, data)


class TranslateType(Enum):
    prova_facil_key_to_client_legacy_key = 1
    client_legacy_key_to_prova_facil_key = 2


class AttributeProcessor:
    TranslateType = TranslateType.client_legacy_key_to_prova_facil_key

    def process_attribute(self, data, attribute, value):
        if value is None:
            return data

        if isinstance(value, str):
            value = value.strip()

        splited = attribute.split('.')
        tmp_data = data

        current_property = None
        for index, tmpattribute in enumerate(splited):
            current_property = objects.AttributeProperty()
            current_property.parse(tmpattribute)

            if current_property.name not in tmp_data:
                tmp_data[current_property.name] = {}

            if index < len(splited) - 1:
                tmp_data = tmp_data[current_property.name]

        if current_property.isarray:
            tmp_data[current_property.name] = []
            for current_value in str(value).split('|'):
                tmp_data[current_property.name].append(self.process_value(current_property.function, current_value))
        else:
            tmp_data[current_property.name] = self.process_value(current_property.function, value)

    def _trans(self, service_name, key):
        value = None

        if self.TranslateType == TranslateType.client_legacy_key_to_prova_facil_key:
            sync_data = models.SyncData.objects.filter(legacy_key=key, query_config__service__name=service_name)
            if sync_data:
                value = sync_data[0].sgp_key
        else:
            sync_data = models.SyncData.objects.filter(sgp_key=key, query_config__service__name=service_name)
            if sync_data:
                value = sync_data[0].legacy_key

        if not value:
            message = 'Service: {} - Chave {} não foi sincronizado anteriormente.'.format(service_name, key)
            LOG.warning(
                msg=message
            )
            raise NotSyncedForTransException(message)

        return value

    def process_value(self, function, value):
        if (not function) or (not value):
            return value

        result = re.search(r'(?P<function>.+)\((?P<parameter>.*)\)', function)
        if not result:
            return value

        function_name = result.group('function')
        parameter = result.group('parameter')
        if function_name == 'int':
            value = int(value)
        elif function_name == 'str':
            value = str(value)
        elif function_name == 'bool':
            value = bool(value)
        elif function_name == 'trans':
            service_name = parameter
            value = self._trans(service_name, value)

        return value
