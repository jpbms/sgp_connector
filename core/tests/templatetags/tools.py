from django.test import TestCase
from core.templatetags import tools


class SplitTestCase(TestCase):
    def test_passing_my_name_should_return_three_string_in_a_list(self):
        my_name = 'Bruno Santiago Pinheiro'

        result = tools.split(my_name, ' ')
        expected = ['Bruno', 'Santiago', 'Pinheiro']

        self.assertEqual(result, expected)


class GetTestCase(TestCase):
    def test_passing_a_iterable_and_a_index_should_return_interable_on_index(self):
        fruits = ['Pen', 'Pineapple', 'Apple']

        result = tools.get(fruits, 0)

        self.assertEqual(result, 'Pen')
