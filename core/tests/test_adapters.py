#!/usr/bin/python
# -*- encoding: utf-8 -*-
from unittest import mock

import pyodbc
from django.test import TestCase

from core import adapters


class RowAdapterTestCase(TestCase):

    @mock.patch('core.adapters.RowAdapter.convert')
    def test_convert_row_to_object_should_call_convert(self, mocked_convert):

        row_adapter = adapters.RowAdapter()

        row = ''

        row_adapter.convert_row_to_object(row)

        mocked_convert.assert_called()

    @mock.patch('core.adapters.RowAdapter.convert')
    def test_convert_row_to_object_should_call_convert_with_row(self, mocked_convert):
        row_adapter = adapters.RowAdapter()

        row = ''

        row_adapter.convert_row_to_object(row)

        mocked_convert.assert_called_with(row)

    @mock.patch('pyodbc.Row', mock.Mock())
    @mock.patch('core.adapters.RowAdapter.process_attribute')
    def test_convert_should_call_process_attribute(self, mocked_process_attribute):
        row_adapter = adapters.RowAdapter()

        row = mock.MagicMock(cursor_description=[['']])

        row.heads.__getitem__ = [1, 2]

        row_adapter.convert(row)

        mocked_process_attribute.assert_called()

    @mock.patch('pyodbc.Row', mock.Mock())
    @mock.patch('core.adapters.RowAdapter.process_attribute')
    def test_convert_should_call_process_attribute_two_times_if_have_two_descriptions(self, mocked_process_attribute):
        row_adapter = adapters.RowAdapter()

        row = mock.MagicMock(cursor_description=[[''], ['']])

        row.heads.__getitem__ = [1, 2]

        row_adapter.convert(row)

        self.assertEqual(2, mocked_process_attribute.call_count)

    @mock.patch('pyodbc.Row', mock.Mock())
    @mock.patch('core.adapters.RowAdapter.process_attribute')
    def test_convert_should_call_process_attribute_with_data_attribute_and_value(self, mocked_process_attribute):
        row_adapter = adapters.RowAdapter()

        description = ''

        row = mock.MagicMock(cursor_description=[[description]])

        row.heads.__getitem__ = [1]

        data = {}

        attribute = description

        value = row[0]

        row_adapter.convert(row)

        mocked_process_attribute.assert_called_with(data, attribute, value)