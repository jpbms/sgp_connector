from django.test import TestCase
from core import models


class SyncDataTestCase(TestCase):
    def test_legacy_key_and_query_config_should_be_unique_together(self):
        meta = models.SyncData._meta
        self.assertIn(('legacy_key', 'query_config'), meta.unique_together)

    def test_sgp_key_and_query_config_should_be_unique_together(self):
        meta = models.SyncData._meta
        self.assertIn(('sgp_key', 'query_config'), meta.unique_together)
