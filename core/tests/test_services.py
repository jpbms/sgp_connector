#!/usr/bin/python
# -*- encoding: utf-8 -*-
from unittest import mock

from django.test import TestCase

from core import services, business, models, exceptions


class IntegrationServiceTestCase(TestCase):

    @mock.patch('core.adapters.RowAdapter')
    @mock.patch('core.business.SendToSGP.get_sync_data_if_exists', mock.Mock(models.SyncData(last_sync_control=None)))
    @mock.patch('django.db.models.query.QuerySet.get', mock.Mock())
    @mock.patch('core.business.SendToSGP._get_legacy_key', mock.Mock())
    @mock.patch('core.business.SendToSGP._get_name', mock.Mock())
    @mock.patch('django.db.models.base.Model.save', mock.Mock())
    @mock.patch('core.business.SendToSGP.integrates_object', mock.Mock())
    @mock.patch('core.business.SendToSGP.send_request_to_sgp', mock.Mock())
    def test_send_objects_to_integrate_should_call_RowAdapter(self, mocked_RowAdapter):
        #Prepare
        integration_service = services.IntegrationService(mock.Mock(), mock.Mock())
        #Act
        integration_service.send_objects_to_integrate(mock.Mock(), business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock()))
        #Assert
        self.assertTrue(mocked_RowAdapter.assert_called)

    @mock.patch('core.adapters.RowAdapter.convert_row_to_object')
    @mock.patch('core.business.SendToSGP.get_sync_data_if_exists', mock.Mock(models.SyncData(last_sync_control=None)))
    @mock.patch('core.business.SendToSGP.update_object', mock.Mock(return_value=('', mock.Mock(last_sync_control=None))))
    def test_send_objects_to_integrate_should_call_convert_row_to_object(self, mocked_convert_row_to_object):
        integration_service = services.IntegrationService(mock.Mock(), mock.Mock())

        integration_service.send_objects_to_integrate(mock.Mock(), business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock()))

        self.assertTrue(mocked_convert_row_to_object.assert_called)

    @mock.patch('core.business.SendToSGP.get_sync_data_if_exists')
    @mock.patch('core.adapters.RowAdapter.convert_row_to_object', mock.Mock(return_value={'legacy_key': 1, 'name': 1}))
    @mock.patch('core.business.SendToSGP.update_object', mock.Mock(return_value=('', mock.Mock(last_sync_control=None))))
    def test_send_objects_to_integrate_should_call_get_sync_data_if_exists(self, mocked_get_sync_data_if_exists):
        integration_service = services.IntegrationService(services.ReportStatusWeb(), mock.Mock())

        integration_service.send_objects_to_integrate(mock.Mock(),
                                                      business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock()))

        self.assertTrue(mocked_get_sync_data_if_exists.assert_called)

    @mock.patch('core.business.SendToSGP.integrates_object')
    @mock.patch('core.adapters.RowAdapter.convert_row_to_object', mock.Mock(return_value={'legacy_key': 1, 'name': 1}))
    @mock.patch('core.business.SendToSGP.get_sync_data_if_exists', mock.Mock(return_value=None))
    def test_send_objects_to_integrate_should_call_integrates_object_if_syncdata_not_exists(self, mocked_integrates_object):
        integration_service = services.IntegrationService(services.ReportStatusWeb(), mock.Mock())

        mocked_integrates_object.return_value = '', mock.Mock(last_sync_control=None)

        integration_service.send_objects_to_integrate(mock.Mock(),
                                                      business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock()))

        self.assertTrue(mocked_integrates_object.assert_called)

    @mock.patch('core.business.SendToSGP.update_object')
    @mock.patch('core.adapters.RowAdapter.convert_row_to_object', mock.Mock(return_value={'legacy_key': 1, 'name': 1}))
    @mock.patch('core.business.SendToSGP.get_sync_data_if_exists', mock.Mock(return_value=mock.Mock()))
    def test_send_objects_to_integrate_should_call_integrates_object_if_syncdata_not_exists(self,
                                                                                            mocked_update_object):
        integration_service = services.IntegrationService(services.ReportStatusWeb(), mock.Mock())

        mocked_update_object.return_value = '', mock.Mock(last_sync_control=None)

        integration_service.send_objects_to_integrate(mock.Mock(),
                                                      business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock()))

        self.assertTrue(mocked_update_object.assert_called)


class MessageBuilderTestCase(TestCase):
    message = '{"message": "Olá {{ name }}!"}'

    def test_message_builder_should_render_django_templates(self):
        message_builder = services.MessagesBuilder(self.message)
        messages = message_builder.build('[{"name": "Bruno"}]', '')

        self.assertEqual(messages, [dict(message="Olá Bruno!")])

    def test_message_builder_should_start_from_list_path(self):
        message_builder = services.MessagesBuilder(self.message)
        messages = message_builder.build('{"data": {"here": [{"name": "Bruno"}]}}', 'data.here')

        self.assertEqual(messages, [dict(message="Olá Bruno!")])

    def test_message_builder_should_raise_message_when_template_is_not_a_valid_json(self):
        message_builder = services.MessagesBuilder(self.message[:-2])

        with self.assertRaises(exceptions.InvalidJson):
            message_builder.build('[{"name": "Bruno"}]', '')
