#!/usr/bin/python
# -*- encoding: utf-8 -*-
import json
from unittest import mock

from django.test import TestCase
from requests import Response

from core import business, models
from core.models import Client


class SendToSGPTestCase(TestCase):

    def test_get_legacy_key_should_return_object_legacy_key(self):
        send_to_sgp = business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock())

        send_to_sgp.obj = {'legacy_key': '123'}

        expected_return = '123'

        legacy_key = send_to_sgp._get_legacy_key()

        self.assertEqual(expected_return, legacy_key)

    def test_get_name_should_return_object_name(self):
        send_to_sgp = business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock())

        send_to_sgp.obj = {'name': 'name'}

        expected_return = 'name'

        name = send_to_sgp._get_name()

        self.assertEqual(expected_return, name)

    @mock.patch('core.business.SendToSGP.send_request_to_sgp')
    @mock.patch('core.business.SendToSGP._get_legacy_key', mock.Mock())
    @mock.patch('core.business.SendToSGP._get_name', mock.Mock())
    def test_integrates_object_should_call_send_request_to_sgp(self, mocked_send_request_to_sgp):
        send_to_sgp = business.SendToSGP(mock.Mock(), models.SyncControl(), Client())

        send_to_sgp.integrates_object()

        self.assertTrue(mocked_send_request_to_sgp.assert_called)

    @mock.patch('core.business.SendToSGP.send_request_to_sgp')
    @mock.patch('core.business.SendToSGP._get_legacy_key', mock.Mock())
    @mock.patch('core.business.SendToSGP._get_name', mock.Mock())
    def test_integrates_object_should_call_send_request_to_sgp_with_request_type_and_obj(self, mocked_send_request_to_sgp):
        send_to_sgp = business.SendToSGP(mock.Mock(), models.SyncControl(), Client())

        request_type = 'POST'

        send_to_sgp.obj = ''

        send_to_sgp.integrates_object()

        mocked_send_request_to_sgp.assert_called_with(request_type=request_type, data='')

    def test_get_sync_data_if_sync_data_exists_should_return_sync_data(self):
        client = models.Client()
        service = models.Service.objects.create(order=1, client=client)
        query_config = models.QueryConfig.objects.create(query='', mode='SEND', client=client, service=service)
        sync_data = models.SyncData.objects.create(legacy_key='123', query_config=query_config, client=client)
        send_to_sgp = business.SendToSGP(query_config.pk, mock.Mock(), client)

        send_to_sgp.obj = {'legacy_key': '123'}

        returned_sync_data = send_to_sgp.get_sync_data_if_exists()

        self.assertEqual(sync_data, returned_sync_data)

    @mock.patch('core.business._get_request_header')
    @mock.patch('core.business.SendToSGP.get_url', mock.Mock())
    @mock.patch('core.business.SendToSGP._get_sgp_key', mock.Mock())
    @mock.patch('core.business.SendToSGP._get_legacy_key', mock.Mock(return_value=''))
    @mock.patch('core.business.SendToSGP._get_name', mock.Mock(return_value=''))
    def test_send_request_to_sgp_should_call_get_request_header(self, mocked_get_request_header):
        send_to_sgp = business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock())

        with mock.patch('core.business.SendToSGP.execute_request') as mocked_execute_request:
            mocked_execute_request.return_value = mock.Mock(status_code=200)

            send_to_sgp.send_request_to_sgp(request_type='', data={})

        self.assertTrue(mocked_get_request_header.assert_called)

    @mock.patch('core.business.SendToSGP.get_url')
    @mock.patch('core.business._get_request_header', mock.Mock())
    @mock.patch('core.business.SendToSGP._get_sgp_key', mock.Mock())
    @mock.patch('core.business.SendToSGP._get_legacy_key', mock.Mock(return_value=''))
    @mock.patch('core.business.SendToSGP._get_name', mock.Mock(return_value=''))
    def test_send_request_to_sgp_should_call_get_url(self, mocked_get_url):
        send_to_sgp = business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock())

        with mock.patch('core.business.SendToSGP.execute_request') as mocked_execute_request:
            mocked_execute_request.return_value = mock.Mock(status_code=200)

            send_to_sgp.send_request_to_sgp(request_type='', data={})

        self.assertTrue(mocked_get_url.assert_called)

    @mock.patch('core.business.SendToSGP._get_sgp_key', mock.Mock())
    @mock.patch('core.business._get_request_header')
    @mock.patch('core.business.SendToSGP.get_url')
    @mock.patch('core.business.SendToSGP.execute_request')
    @mock.patch('core.business.SendToSGP._get_legacy_key', mock.Mock(return_value=''))
    @mock.patch('core.business.SendToSGP._get_name', mock.Mock(return_value=''))
    def test_send_request_to_sgp_should_call_execute_request_with_valid_parameters(self, mocked_execute_request, mocked_get_url,
                                                                                   mocked_get_request_header):
        data = {}
        request_type = mock.Mock()

        url = mock.Mock()
        mocked_get_url.return_value = url

        headers = mock.Mock()
        mocked_get_request_header.return_value = headers

        mocked_execute_request.return_value = mock.MagicMock(status_code=200)
        send_to_sgp = business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock())
        send_to_sgp.send_request_to_sgp(request_type=request_type, data=data)

        expected_data = json.dumps(data)
        mocked_execute_request.assert_called_with(request_type, url, headers, expected_data)

    def test_get_url_should_return_query_config_url_if_request_type_is_post(self):
        client = models.Client()
        query_config_id = 1
        expected_url = 'url'
        request_type = 'POST'
        service = models.Service.objects.create(client=client, name='', is_active=True, order=1)
        models.QueryConfig.objects.create(id=query_config_id, service_url=expected_url, client=client, service=service)
        send_to_sgp = business.SendToSGP(query_config_id, mock.Mock(), client)
        returned_url = send_to_sgp.get_url(request_type=request_type)

        self.assertEqual(expected_url, returned_url)

    def test_get_url_should_return_query_config_url_plus_sgp_key_if_request_type_is_patch(self):
        client = models.Client()
        query_config_id = 1
        sgp_key = '2'
        url = 'url/'
        request_type = 'PATCH'
        service = models.Service.objects.create(order=1, client=client)
        models.QueryConfig.objects.create(id=query_config_id, service_url=url, client=client, service=service)
        send_to_sgp = business.SendToSGP(query_config_id, mock.Mock(), client)
        returned_url = send_to_sgp.get_url(request_type=request_type, sgp_key=sgp_key)

        expected_url = url+sgp_key+'/'

        self.assertEqual(expected_url, returned_url)

    @mock.patch('core.models.SyncData.activate_sync_data')
    @mock.patch('django.db.models.query.QuerySet.get', mock.Mock(return_value=models.QueryConfig(update_data=True, update_status=True)))
    @mock.patch('core.business.SendToSGP._get_legacy_key', mock.Mock())
    @mock.patch('core.business.SendToSGP._get_name', mock.Mock())
    @mock.patch('core.business.SendToSGP.send_request_to_sgp', mock.Mock())
    def test_update_object_should_call_activate_sync_data_if_update_status_is_active_and_sync_data_is_data_active_and_obj_does_not_has_status(self, mocked_activate_sync_data):
        #Prepare
        sgp_key = 'batata'
        syncdata = models.SyncData(is_data_active=True)
        send_to_sgp = business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock())
        send_to_sgp.obj = mock.Mock()
        #Act
        send_to_sgp.update_object(sgp_key, syncdata)
        #Asssert
        self.assertTrue(mocked_activate_sync_data.assert_called)

    @mock.patch('core.business.SendToSGP.send_request_to_sgp')
    @mock.patch('django.db.models.query.QuerySet.get',
                mock.Mock(return_value=models.QueryConfig(update_data=True, update_status=True)))
    @mock.patch('core.business.SendToSGP._get_legacy_key', mock.Mock())
    @mock.patch('core.business.SendToSGP._get_name', mock.Mock())
    def test_update_object_should_call_send_request_to_sgp(self, mocked_send_request_to_sgp):
        sgp_key = 'batata'
        syncdata = models.SyncData(is_data_active=True)
        send_to_sgp = business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock())
        send_to_sgp.obj = mock.Mock()
        # Act
        send_to_sgp.update_object(sgp_key, syncdata)
        # Asssert
        self.assertTrue(mocked_send_request_to_sgp.assert_called)

    @mock.patch('core.business.SendToSGP.send_request_to_sgp')
    @mock.patch('django.db.models.query.QuerySet.get',
                mock.Mock(return_value=models.QueryConfig(update_data=True, update_status=True)))
    @mock.patch('core.business.SendToSGP._get_legacy_key', mock.Mock())
    @mock.patch('core.business.SendToSGP._get_name', mock.Mock())
    def test_update_object_should_call_send_request_to_sgp_with_request_type_and_data_and_sgp_key(self, mocked_send_request_to_sgp):
        sgp_key = 'batata'
        syncdata = models.SyncData(is_data_active=True)
        send_to_sgp = business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock())
        send_to_sgp.obj = {}
        # Act
        send_to_sgp.update_object(sgp_key, syncdata)
        # Asssert
        expected_request_type = 'PATCH'
        expected_data = {}
        expected_sgp_key = 'batata'
        mocked_send_request_to_sgp.assert_called_with(request_type=expected_request_type, data=expected_data, sgp_key=expected_sgp_key)

    def test_get_key_identified_in_response_should_return_field_id_when_response_it_has_id(self):
        id_expected = '123'
        response_json = {"id": id_expected}

        id_result = business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock())._get_key_identified_in_response(response_json)

        self.assertEqual(id_expected, id_result)

    def test_get_key_identified_in_response_should_return_field_key_when_response_it_has_key(self):
        key_expected = '123'
        response_json = {"key": key_expected}

        key_result = business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock())._get_key_identified_in_response(response_json)

        self.assertEqual(key_expected, key_result)

    @mock.patch('core.business.SendToSGP._get_key_identified_in_response', mock.Mock(return_value=None))
    def test_get_key_should_return_fake_sgp_key_when_get_sgp_key_in_response_return_none(self):
        fake_sgp_key_expected = '123'
        response = mock.Mock(spec=Response)
        send_to_sgp = business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock())
        send_to_sgp.obj = {"fake_sgp_key": fake_sgp_key_expected}

        fake_sgp_key_result = send_to_sgp._get_key(response, '')

        self.assertEqual(fake_sgp_key_expected, fake_sgp_key_result)

    @mock.patch('core.business.SendToSGP._get_key_identified_in_response', mock.Mock(return_value=None))
    def test_get_key_should_raise_exception_when_get_sgp_key_in_response_return_none(self):
        response = mock.Mock(spec=Response, text='Texto de erro')
        response_text = response.text
        response.json.return_value = {}
        send_to_sgp = business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock())
        send_to_sgp.obj = {}

        message_expected = "Chave não foi encontrada no dado retornado: " + response_text
        with self.assertRaisesMessage(Exception, message_expected):
            send_to_sgp._get_key(response, response_text)

    @mock.patch('core.business.SendToSGP._get_key')
    def test_get_sgp_key_should_raise_exception_when_get_key_raise_exception(self, mocked_get_key):
        response = mock.Mock(spec=Response, text='Texto de erro')
        response.json.return_value = {}
        send_to_sgp = business.SendToSGP(mock.Mock(), mock.Mock(), mock.Mock())
        send_to_sgp.obj = {}
        exception = Exception("Teste Excpection")
        mocked_get_key.side_effect = exception

        message_expected = 'Erro ao processar response: {}'.format(str(exception))
        with self.assertRaisesMessage(Exception, message_expected):
            send_to_sgp._get_sgp_key(response)

    @mock.patch('core.business.SendToSGP._is_reuse_key_if_duplicaded', mock.Mock(return_value=False))
    def test_can_register_on_sync_data_should_return_true_when_status_code_is_200(self):
        status_code = 200

        can_register_on_sync_data_result = business.SendToSGP(
            mock.Mock(), mock.Mock(), mock.Mock())._can_register_on_sync_data(status_code)

        self.assertTrue(can_register_on_sync_data_result)

    @mock.patch('core.business.SendToSGP._is_reuse_key_if_duplicaded', mock.Mock(return_value=False))
    def test_can_register_on_sync_data_should_return_true_when_status_code_is_201(self):
        status_code = 201

        can_register_on_sync_data_result = business.SendToSGP(
            mock.Mock(), mock.Mock(), mock.Mock())._can_register_on_sync_data(status_code)

        self.assertTrue(can_register_on_sync_data_result)

    @mock.patch('core.business.SendToSGP._is_reuse_key_if_duplicaded', mock.Mock(return_value=True))
    def test_can_register_on_sync_data_should_return_true_when_status_code_is_different_of_200_or_201_but_is_reuse_key_if_duplicaded_is_true(self):
        status_code = 'lala'

        can_register_on_sync_data_result = business.SendToSGP(
            mock.Mock(), mock.Mock(), mock.Mock())._can_register_on_sync_data(status_code)

        self.assertTrue(can_register_on_sync_data_result)

    @mock.patch('django.db.models.query.QuerySet.get')
    def test_is_reuse_key_if_duplicaded_should_return_true_when_attribute_reuse_key_if_duplicated_of_query_config_is_true_and_attribute_received_is_409(
            self, mocked_get):
        reuse_key_if_duplicated = True
        status_code = 409
        mocked_get.return_value = mock.Mock(spec=models.QueryConfig, reuse_key_if_duplicated=reuse_key_if_duplicated)

        is_reuse_key_if_duplicaded_result = business.SendToSGP(
            mock.Mock(), mock.Mock(), mock.Mock())._is_reuse_key_if_duplicaded(status_code)

        self.assertTrue(is_reuse_key_if_duplicaded_result)

    @mock.patch('django.db.models.query.QuerySet.get')
    def test_is_reuse_key_if_duplicaded_should_return_false_when_attribute_reuse_key_if_duplicated_of_query_config_is_false_and_attribute_received_is_409(
            self, mocked_get):
        reuse_key_if_duplicated = False
        status_code = 409
        mocked_get.return_value = mock.Mock(spec=models.QueryConfig, reuse_key_if_duplicated=reuse_key_if_duplicated)

        is_reuse_key_if_duplicaded_result = business.SendToSGP(
            mock.Mock(), mock.Mock(), mock.Mock())._is_reuse_key_if_duplicaded(status_code)

        self.assertFalse(is_reuse_key_if_duplicaded_result)

    @mock.patch('django.db.models.query.QuerySet.get')
    def test_is_reuse_key_if_duplicaded_should_return_false_when_attribute_reuse_key_if_duplicated_of_query_config_is_true_and_attribute_received_is_not_409(
            self, mocked_get):
        reuse_key_if_duplicated = False
        status_code = 500
        mocked_get.return_value = mock.Mock(spec=models.QueryConfig, reuse_key_if_duplicated=reuse_key_if_duplicated)

        is_reuse_key_if_duplicaded_result = business.SendToSGP(
            mock.Mock(), mock.Mock(), mock.Mock())._is_reuse_key_if_duplicaded(status_code)

        self.assertFalse(is_reuse_key_if_duplicaded_result)


class GetRequestHeaderTestCase(TestCase):

    def test_get_request_header_should_return_dict_with_token_and_content_type(self):
        token = '00000'
        client = Client(token=token)
        expected_dict = {'Authorization': 'Token ' + token, 'Content-type': 'application/json'}

        returned_header = business._get_request_header(client)

        self.assertDictEqual(expected_dict, returned_header)
