from django.test import TestCase

# Create your tests here.
from core import models
from core.adapters import RowAdapter
from core.models import QueryConfig, SyncData


class RowAdapterTest(TestCase):

    def test_processSpecialAttibute_Subattributes(self):
        row = RowAdapter()
        data = {}
        data = row.process_attribute(data, 'teste.alo', 'toaqui')

        self.assertEqual('toaqui', data["teste"]["alo"])

    def test_processSpecialAttibute_arrayattribute(self):
        row = RowAdapter()
        data = {}
        data = row.process_attribute(data, 'teste.alo[]', 'toaqui|toaqui2')

        self.assertEqual(['toaqui', 'toaqui2'], data["teste"]["alo"])

    def test_processSpecialAttibute_arrayattribute_integer(self):
        row = RowAdapter()
        data = {}
        data = row.process_attribute(data, 'teste.alo[]:int()', '1|2')

        self.assertEqual([1, 2], data["teste"]["alo"])

    def test_processvalue_no_function_return_same_value(self):
        row = RowAdapter()
        result = row.process_value('teste', 1)
        self.assertEqual(1, result)

    def test_processvalue_int_1_string_return_1_int(self):
        row = RowAdapter()
        result = row.process_value('int()', '1')
        self.assertEqual(1, result)

    def test_processvalue_str_1_int_return_1_str(self):
        row = RowAdapter()
        result = row.process_value('str()', 1)
        self.assertEqual('1', result)

    def test_processvalue_trans_Academic_mat_return_mat_sgp_key(self):
        client = models.Client()
        service = models.Service(name='Academic', order=0, client=client)
        service.save()
        queryconfig = QueryConfig()
        queryconfig.client = client
        queryconfig.service = service
        queryconfig.save()

        syncData = SyncData()
        syncData.legacy_key = 'mat'
        syncData.sgp_key = 'mat_sgp_key'
        syncData.query_config = queryconfig
        syncData.client = client
        syncData.save()

        row = RowAdapter()
        result = row.process_value('trans(Academic)', 'mat')
        self.assertEqual('mat_sgp_key', result)
