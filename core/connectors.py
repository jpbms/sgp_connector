# -*- encoding: utf-8 -*-
# !/usr/bin/python
import logging

import pyodbc

from django.conf import settings

from core import models
from core import constants

LOG = logging.getLogger(constants.DEFAULT_LOGGER)


class ConnectorODBC(object):

    def __init__(self, client):
        self.client = client
        self.connection = self.connect_in_odbc()

    def connect_in_odbc(self):

        connection_string = self.client.connection_string
        connection = pyodbc.connect(connection_string)

        LOG.info(
            msg='O client {} estabeleceu conexão'.format(self.client),
            extra={'connection': connection, 'connection_string': connection_string}
        )

        return connection

    def execute_query(self, query):
        cursor = self.connection.cursor()
        try:
            data = cursor.execute(query)
            # quando é uma consulta, deve utilizar o fetchall() para não ficar com conexão aberta
            if "select" in query.lower():
                data = data.fetchall()
            return data
        except Exception as e:
            LOG.error(
                msg='Falha na execução da query', exc_info=True, extra={'connection_cursor': cursor, 'query': query}
            )
            raise Exception(e)

    def commit(self):
        return self.connection.cursor().commit()
