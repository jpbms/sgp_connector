import hashlib
import json
import logging
import uuid

from django.http import HttpResponse, Http404
from django.shortcuts import render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from core import models, services, constants

LOG = logging.getLogger(constants.DEFAULT_LOGGER)


def dashboard_index(request, client_slug):
    try:
        client = models.Client.objects.get(client_slug=client_slug)

    except models.Client.DoesNotExist:
        LOG.warning(msg="O client slug '{}' não existe".format(client_slug), exc_info=True)
        raise Http404

    last_sync = models.SyncControl.objects.filter(client=client).order_by('end').last()
    services_with_error = None
    services_with_success = None

    if last_sync:
        type_error = models.SyncLog.ERROR
        services_with_error = models.SyncLog.objects.filter(sync_id=last_sync.id, type=type_error, client=client)

        type_info = models.SyncLog.INFO
        services_with_success = models.SyncLog.objects.filter(sync_id=last_sync.id, type=type_info, client=client)

    last_sync_list = models.SyncData.objects.filter(client=client).order_by('-id')[:10]
    query_config_list = models.QueryConfig.objects.filter(service__is_active=True, client=client).\
        order_by('service__order')
    web_configs = models.WebConfig.objects.filter(service__is_active=True, client=client)

    context = {
        'client': client,
        'last_sync': last_sync,
        'services_with_error': services_with_error,
        'services_with_success': services_with_success,
        'last_sync_list': last_sync_list,
        'query_config_list': query_config_list,
        'web_configs': web_configs,
    }

    return render(request, 'core/index.html', context)


class SyncDataWebConfig(TemplateView):
    template_name = 'core/sync_data_web_config.html'

    def dispatch(self, request, *args, **kwargs):
        self.web_config = get_object_or_404(models.WebConfig, id=kwargs.get('pk'))
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        client = models.Client.objects.get(client_slug=kwargs.get('client_slug'))

        reporter = services.ReportStatusWeb()
        reporter.start(client)

        syncer = services.WebConfigSyncer(self.web_config, reporter, client)
        syncer.sync()

        context['report_full_text'] = reporter.fulltext

        return context


def sync_all(request, client_slug):
    client = models.Client.objects.get(client_slug=client_slug)
    reporter = services.ReportStatusWeb()
    reporter.start(client)

    integration = services.IntegrationService(reporter, client)
    integration.execute(None)

    web_configs = models.WebConfig.objects.filter(client=client, service__is_active=True)
    for web_config in web_configs:
        syncer = services.WebConfigSyncer(web_config, reporter)
        syncer.sync()

    return HttpResponse(reporter.fulltext)


def execute_sync_data(request, client_slug, query_config_id):
    client = models.Client.objects.get(client_slug=client_slug)
    report = services.ReportStatusWeb()
    report.start(client)

    integration = services.IntegrationService(report, client)

    integration.execute(query_config_id)

    return HttpResponse(report.fulltext)


def get_sync_data_status(request, client_slug, limit=0):
    data = services.ReportStatusWeb.logdata
    if limit and services.ReportStatusWeb.client and services.ReportStatusWeb.client.client_slug == client_slug:
        data = data[:limit]
    else:
        data = ''

    text = '<br/>'.join(data)
    return HttpResponse(text)


@csrf_exempt
def generate_user_token(request_data, expiration_date):
    m = hashlib.md5(str(request_data["username"] + expiration_date).encode("utf-8"))
    return m.hexdigest()


@csrf_exempt
def token_generate(request, client_slug):
    try:
        client = models.Client.objects.get(client_slug=client_slug)

    except models.Client.DoesNotExist:
        LOG.warning(msg="O client slug '{}' não existe".format(client_slug), exc_info=True)
        raise Http404

    request_data = json.loads(request.body)
    ssoToken = models.SSOToken()
    ssoToken.token = str(uuid.uuid1())
    ssoToken.client = client
    ssoToken.username = request_data["username"]
    ssoToken.module_login = request_data["module_login"]
    ssoToken.save()

    response = {
                   "token": ssoToken.token,
                   "expiration_date": "1554552000",
    }

    return HttpResponse(json.dumps(response))


def token_validade(request):
    ssotoken = models.SSOToken.objects.get(token=request.GET.get('token'))
    responsetext = ''' <cas:serviceResponse xmlns:cas="http://www.yale.edu/tp/cas">
            <cas:authenticationSuccess>
            <cas:user>{}</cas:user>
            <cas:proxyGrantingTicket>{}</cas:proxyGrantingTicket>
            </cas:authenticationSuccess>
            </cas:serviceResponse>'''.format(ssotoken.username, ssotoken.token)
    return HttpResponse(responsetext)

