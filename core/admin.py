from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.models import User, Group
from django.forms import TextInput
from django.db import models as db_models

from core import models


class ClientAdmin(admin.ModelAdmin):
    list_display = ('name', 'client_slug')
    list_filter = ['client_slug']
    formfield_overrides = {
        db_models.CharField: {'widget': TextInput(attrs={'size': '50%'})},
    }

    def get_queryset(self, request):
        client = request.user.groups.first()

        if client:
            return models.Client.objects.filter(client_slug=client.name)
        else:
            return models.Client.objects.all()


class ServiceAdmin(admin.ModelAdmin):
    list_filter = ['client', 'is_active']

    def get_queryset(self, request):
        return models.Service.objects.all()


class WebConfigAdmin(admin.ModelAdmin):
    list_filter = ['client', 'service']

    def get_queryset(self, request):
        return models.WebConfig.objects.all()

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['service', 'client']
        else:
            return []


class QueryConfigAdmin(admin.ModelAdmin):
    list_display = ('service', 'client')
    list_filter = ['client']
    formfield_overrides = {
        db_models.CharField: {'widget': TextInput(attrs={'size': '100%'})},
    }

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['service', 'client']
        else:
            return []

    def get_queryset(self, request):
        client = request.user.groups.first()

        if client:
            client = models.Client.objects.get(client_slug=client.name)
            return models.QueryConfig.objects.filter(client=client).order_by('service__order')
        else:
            return models.QueryConfig.objects.all().order_by('service__order')


class SyncDataAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'legacy_key', 'sgp_key')
    list_filter = ['client', 'query_config']

    def get_queryset(self, request):
        client = request.user.groups.first()

        if client:
            client = models.Client.objects.get(client_slug=client.name)
            return models.SyncData.objects.filter(client=client)
        else:
            return models.SyncData.objects.all()


class SyncControlAdmin(admin.ModelAdmin):
    list_display = ('start', 'end')
    list_filter = ['client', 'queries_config']

    def get_queryset(self, request):
        client = request.user.groups.first()

        if client:
            client = models.Client.objects.get(client_slug=client.name)
            return models.SyncControl.objects.filter(client=client)
        else:
            return models.SyncControl.objects.all()


class WebSyncControlAdmin(admin.ModelAdmin):
    list_display = ('start', 'end')
    list_filter = ['client', 'web_configs']

    def get_queryset(self, request):
        client = request.user.groups.first()

        if client:
            client = models.Client.objects.get(client_slug=client.name)
            return models.WebSyncControl.objects.filter(client=client)
        else:
            return models.WebSyncControl.objects.all()


class SyncLogAdmin(admin.ModelAdmin):
    list_display = ('sync_id', 'type', 'message')
    list_filter = ['client', 'type']

    def get_queryset(self, request):
        client = request.user.groups.first()

        if client:
            client = models.Client.objects.get(client_slug=client.name)
            return models.SyncLog.objects.filter(client=client)
        else:
            return models.SyncLog.objects.all()


class WebSyncLogAdmin(admin.ModelAdmin):
    list_display = ('sync_id', 'type', 'message')
    list_filter = ['client', 'type']

    def get_queryset(self, request):
        client = request.user.groups.first()

        if client:
            client = models.Client.objects.get(client_slug=client.name)
            return models.WebSyncLog.objects.filter(client=client)
        else:
            return models.WebSyncLog.objects.all()


class ProfileUserAdmin(UserAdmin):
    form = UserChangeForm

    def get_queryset(self, request):
        client = request.user.groups.first()

        if client:
            return User.objects.none()
        else:
            return User.objects.all()


class GroupsAdmin(admin.ModelAdmin):

    def get_queryset(self, request):
        client = request.user.groups.first()

        if client:
            return Group.objects.none()
        else:
            return Group.objects.all()


admin.site.register(models.Client, ClientAdmin)
admin.site.register(models.Service, ServiceAdmin)
admin.site.register(models.WebConfig, WebConfigAdmin)
admin.site.register(models.WebSyncControl, WebSyncControlAdmin)
admin.site.register(models.WebSyncLog, WebSyncLogAdmin)
admin.site.register(models.QueryConfig, QueryConfigAdmin)
admin.site.register(models.SyncData, SyncDataAdmin)
admin.site.register(models.SyncControl, SyncControlAdmin)
admin.site.register(models.SyncLog, SyncLogAdmin)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.unregister(Group)
admin.site.register(Group, GroupsAdmin)
