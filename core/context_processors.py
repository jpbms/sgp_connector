#!/usr/bin/python
# -*- encoding: utf-8 -*-
from django.conf import settings


def system_settings_variables(request):
    config = {
        'configs': {
            "VERSION": settings.VERSION,
        }
    }
    return config
