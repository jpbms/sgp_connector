from django import template

register = template.Library()


def split(value, separator):
    return value.split(separator)


def get(value, index):
    return value[int(index)]


register.filter('split', split)
register.filter('get', get)
