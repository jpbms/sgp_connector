# -*- coding: UTF-8 -*-


class AttributeProperty(object):
    name = None
    isarray = False
    function = None

    def parse(self, text):
        self.name = text

        if '.' in text:
            splited = text.split('.')
            self.name = splited[0]
            self.is_obj = True

        if ':' in text:
            splited = text.split(':')
            self.name = splited[0]
            self.function = splited[1]

        if self.name.endswith('[]'):
            self.isarray = True
            self.name = self.name[:-2]
