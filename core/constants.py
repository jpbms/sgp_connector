#!/usr/bin/python
# -*- encoding: utf-8 -*-


class EnumSgpStatus(object):
    active = '1'
    inactive = '2'


class EnumStatus(object):
    active = True
    inactive = False


DEFAULT_LOGGER = 'sentry'
