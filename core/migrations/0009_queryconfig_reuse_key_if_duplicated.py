# Generated by Django 2.0.7 on 2019-03-20 00:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_syncdata_last_sync_control'),
    ]

    operations = [
        migrations.AddField(
            model_name='queryconfig',
            name='reuse_key_if_duplicated',
            field=models.BooleanField(default=False),
        ),
    ]
