# Generated by Django 2.0.7 on 2019-03-13 21:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20190131_1457'),
    ]

    operations = [
        migrations.AddField(
            model_name='syncdata',
            name='last_sync_control',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='core.SyncControl'),
        ),
    ]
