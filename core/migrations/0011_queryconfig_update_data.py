# Generated by Django 2.0.7 on 2019-04-10 13:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_ssotoken'),
    ]

    operations = [
        migrations.AddField(
            model_name='queryconfig',
            name='update_data',
            field=models.BooleanField(default=False),
        ),
    ]
