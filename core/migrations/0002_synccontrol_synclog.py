# Generated by Django 2.0.7 on 2019-01-14 00:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SyncControl',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start', models.DateTimeField(auto_now=True, verbose_name='Data de inicio')),
                ('end', models.DateTimeField(verbose_name='Data de inicio')),
                ('queries_config', models.ManyToManyField(to='core.QueryConfig')),
            ],
        ),
        migrations.CreateModel(
            name='SyncLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(choices=[('I', 'Informação'), ('W', 'Alerta'), ('E', 'Erro')], max_length=1)),
                ('message', models.TextField()),
                ('query_config', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.QueryConfig')),
                ('sync', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.SyncControl')),
            ],
        ),
    ]
