# Generated by Django 2.0.7 on 2019-04-18 13:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0015_auto_20190416_1832'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClientSqlConnection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('last_update', models.DateTimeField(auto_now=True, null=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, null=True)),
                ('driver', models.CharField(max_length=50, verbose_name='Tipo de banco')),
                ('server', models.CharField(max_length=255, verbose_name='Ip do Servidor')),
                ('database', models.CharField(max_length=100, verbose_name='Nome do banco')),
                ('uid', models.CharField(max_length=100, verbose_name='Id do usuário')),
                ('pwd', models.CharField(max_length=100, verbose_name='Senha do usuário')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Client', verbose_name='Cliente')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
