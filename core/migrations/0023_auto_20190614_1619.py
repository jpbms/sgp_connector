# Generated by Django 2.0.7 on 2019-06-14 19:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0022_websynccontrol_websynclog'),
    ]

    operations = [
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('last_update', models.DateTimeField(auto_now=True, null=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True, null=True)),
                ('order', models.IntegerField()),
                ('name', models.CharField(max_length=100)),
                ('is_active', models.BooleanField(default=True)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Client', verbose_name='Cliente')),
            ],
            options={
                'ordering': ('order',),
            },
        ),
        migrations.AlterField(
            model_name='syncdata',
            name='is_data_active',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='queryconfig',
            name='service',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='core.Service'),
        ),
        migrations.AddField(
            model_name='webconfig',
            name='service',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='core.Service'),
        ),
    ]
