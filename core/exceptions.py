from django.utils.translation import ugettext as _


class NotSyncedForTransException(Exception):
    pass


class InvalidJson(Exception):
    message = _("O template deve ser um JSON válido")

    def __init__(self):
        super(InvalidJson, self).__init__(self.message)
