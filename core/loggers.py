# -*- coding: UTF-8 -*-

from core import models


class SyncLog(object):
    model_log = models.SyncLog

    def __init__(self, sync_id, client):
        self.sync_id = sync_id
        self.client = client

    def register_info(self, query_config, message):
        type = self.model_log.INFO
        return self.register_log(query_config, message, type)

    def register_warning(self, query_config, message):
        type = self.model_log.WARNING
        return self.register_log(query_config, message, type)

    def register_error(self, query_config, message):
        type = self.model_log.ERROR
        return self.register_log(query_config, message, type)

    def register_log(self, query_config, message, type):
        return self.model_log.objects.create(
            sync_id=self.sync_id, query_config=query_config, type=type, message=message, client=self.client)


class WebSyncLog(SyncLog):
    model_log = models.WebSyncLog
