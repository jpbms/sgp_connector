#!/usr/bin/python
# -*- encoding: utf-8 -*-
import json
import logging
from datetime import datetime

import requests
from django.template import Template, Context

from core import adapters, business, connectors, loggers, models, constants, exceptions

LOG = logging.getLogger(constants.DEFAULT_LOGGER)


class IntegrationService(object):
    def __init__(self, report_status, client):
        self.ReportStatus = report_status
        self.client = client

    def execute(self, query_config_id=None):
        # @todo Separar corretamente entre o service e o business.

        queries_config = models.QueryConfig.objects.filter(service__is_active=True, client=self.client)
        if query_config_id:
            queries_config = models.QueryConfig.objects.filter(pk=query_config_id, client=self.client)

        if queries_config.exists():
            sync = models.SyncControl.objects.create(start=datetime.now(), client=self.client)

            for query_config in queries_config:
                sync.queries_config.add(query_config)

                if query_config.send_to_sgp:
                    self.execute_send_to_sgp(sync, query_config)
                elif query_config.receive_from_sgp:
                    self.execute_receive_from_sgp(sync, query_config)

            sync.refresh_from_db()
            sync.end = datetime.now()
            sync.save()

    def execute_send_to_sgp(self, sync, query_config):
        sync_log = self.get_sync_log(sync.id)

        try:
            rows = self.get_query_rows(query_config)

            sgp_keys = []

            integrator = business.SendToSGP(query_config.id, sync, self.client)
            for index, row in enumerate(rows):
                try:
                    if query_config.update_status:
                        sgp_keys.append(self.send_objects_to_integrate(row, integrator))
                    else:
                        self.send_objects_to_integrate(row, integrator)

                except Exception as e:
                    message = 'Erro na integração do dado {} - {}'.format(row, e)
                    LOG.error(
                        msg=message, exc_info=True,
                        extra={'client': self.client, 'sync_id': sync.id, 'query_config': query_config}
                    )
                    self.ReportStatus.report('Erro 2: ' + str(e))
                    sync_log.register_error(query_config, message=message)

            if query_config.update_status:
                self.send_sync_data_to_update_status(query_config, sgp_keys, integrator, sync)

            LOG.info(
                msg='Query {} para integração de envio de dados executada com sucesso'.format(query_config.id),
                extra={'client': self.client, 'query_config': query_config, 'sync_control': sync}
            )
            self.ReportStatus.report('Fim da integração - {}'.format(query_config))
            sync_log.register_info(query_config, message='Fim da integração - {}'.format(query_config))

        except Exception as e:
            LOG.error(
                msg='Erro no envio de dados para o SGP', exc_info=True,
                extra={'client': self.client, 'sync_id': sync.id, 'query_config': query_config}
            )
            self.ReportStatus.report('ERROR: {}'.format(e))
            sync_log.register_error(query_config, e)

    def send_sync_data_to_update_status(self, query_config, sgp_keys, integrator, sync):
        sync_datas = models.SyncData.objects.filter(query_config=query_config,
                                                    is_data_active=constants.EnumStatus.active, client=self.client).\
                                            exclude(sgp_key__in=sgp_keys, last_sync_control=sync)
        if sync_datas.exists():
            for sync_data in sync_datas:
                sgp_key = sync_data.sgp_key
                data = {'status': constants.EnumSgpStatus.inactive}
                integrator.send_request_to_sgp(request_type='PATCH', data=data, sgp_key=sgp_key)
                sync_data.inactivate_sync_data()

                LOG.info(
                    msg='O dado de chave igual a {} foi inativado'.format(sync_data.id),
                    extra={'client': self.client, 'query_config': query_config, 'sync_data': sync_data}
                )

    def send_objects_to_integrate(self, row, integrator):
        processor = adapters.RowAdapter()

        obj = processor.convert_row_to_object(row)

        self.ReportStatus.report('Sincronizando: {} {}'.format(obj["legacy_key"], obj["name"]))

        integrator.obj = obj

        syncdata = integrator.get_sync_data_if_exists()

        if not syncdata:
            sgp_key, syncdata = integrator.integrates_object()
        else:
            sgp_key, syncdata = integrator.update_object(syncdata.sgp_key, syncdata)

        syncdata.last_sync_control = integrator.sync_control
        syncdata.save()

        LOG.info(
            msg='O dado de chave igual a {} foi sincronizado'.format(obj['legacy_key']),
            extra={'client': self.client, 'obj_name': obj['name'], 'obj': obj, 'sync_data': syncdata}
        )

        return sgp_key

    def get_query_rows(self, query_config):
        connection = connectors.ConnectorODBC(self.client)
        rows = connection.execute_query(query_config.query)
        return rows

    def execute_receive_from_sgp(self, sync, query_config):
        sync_log = self.get_sync_log(sync.id)

        try:
            integrator = business.ReceiveFromSGP(query_config, self.client, report=self.ReportStatus, sync_log=sync_log)
            integrator.execute()

            LOG.info(
                msg='Query {} para integração de recuperação de dados executada com sucesso'.format(query_config.id),
                extra={'client': self.client, 'query_config': query_config, 'sync_control': sync}
            )
            self.ReportStatus.report('Fim da integração - {}'.format(query_config))
            sync_log.register_info(query_config, message='Fim da integração - {}'.format(query_config))

        except Exception as e:
            LOG.error(
                msg='Falha na execução da query para integração', exc_info=True,
                extra={'client': self.client, 'sync_id': sync.id, 'query_config': query_config}
            )
            self.ReportStatus.report('ERROR: {}'.format(e))
            sync_log.register_error(query_config, message=e)

    def get_sync_log(self, sync_id):
        return loggers.SyncLog(sync_id=sync_id, client=self.client)


class ReportStatus(object):
    inc = 0

    def report(self, text):
        self.inc = self.inc + 1
        pass


class ReportStatusConsole(ReportStatus):
    def report(self, text):
        super().report(text)
        print(str(self.inc) + " - " + text)


class ReportStatusWeb(ReportStatusConsole):
    fulltext = ""
    logdata = []
    client = None

    def start(self, client):
        ReportStatusWeb.fulltext = ""
        ReportStatusWeb.logdata = []
        ReportStatusWeb.client = client

    def report(self, text):
        super().report(text)
        ReportStatusWeb.logdata.insert(0, str(self.inc) + " - " + text)
        ReportStatusWeb.fulltext = str(self.inc) + " - " + text + "<br/>\r\n" + ReportStatusWeb.fulltext


class MessagesBuilder(object):
    def __init__(self, template):
        base_template = self._get_base_template()
        self.template = Template(base_template + template)

    def _get_base_template(self):
        return """{% load tools %}
        """

    def build(self, message, list_path=None):
        json_message = json.loads(message)

        if list_path:
            crumbs = list_path.split('.')

            for crumb in crumbs:
                json_message = json_message[crumb]

        try:
            messages = []
            for data in json_message:
                message = self.template.render(Context(data))
                messages.append(json.loads(message))
        except json.JSONDecodeError:
            raise exceptions.InvalidJson()

        return messages

    def build_message_with_routine(self, message):
        message = self.template.render(Context(message))

        return message.strip()


class MessagesSender(object):
    def __init__(self, send_to, reporter, logger, web_config):
        self.send_to = send_to
        self.reporter = reporter
        self.web_config = web_config
        self.logger = logger

    def send(self, messages):
        for message in messages:
            response = requests.post(self.send_to, data=message)
            if response.status_code == 200:
                self.reporter.report('Sucesso - %s' % message)
                self.logger.register_info(self.web_config, message)
            else:
                self.reporter.report('Erro - %s' % message)
                self.logger.register_error(self.web_config, message)


class WebConfigSyncer(object):
    def __init__(self, web_config, reporter, client):
        self.web_config = web_config
        self.reporter = reporter
        self.client = client

    def sync(self):
        control = models.WebSyncControl.objects.create(client=self.client, start=datetime.now())
        control.web_configs.add(self.web_config)

        logger = loggers.WebSyncLog(control.id, self.client)

        self.reporter.report("Sincronização iniciada: %s" % self.web_config.name)

        try:
            response = requests.get(self.web_config.sgp_url, params=dict(query=self.web_config.sgp_request_body))

            messages_builder = MessagesBuilder(self.web_config.service_request_body)
            messages = messages_builder.build(response.text, self.web_config.sgp_path_to_list)

            message_sender = MessagesSender(self.web_config.service_url, self.reporter,  logger, self.web_config)
            message_sender.send(messages)
        except Exception as e:
            self.reporter.report("Exceção: %s" % e)
            logger.register_error(self.web_config, e)
        finally:
            control.end = datetime.now()
            control.save()
            self.reporter.report("Sincronização finalizada: %s" % self.web_config.name)
