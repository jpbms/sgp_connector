#!/usr/bin/python
# -*- encoding: utf-8 -*-
import json
import logging
import re

import requests

from core import models, connectors, constants
from core.adapters import TextTradutor, AttributeProcessor, TranslateType
from core.exceptions import NotSyncedForTransException

LOG = logging.getLogger(constants.DEFAULT_LOGGER)


class SendToSGP(object):

    def __init__(self, query_config_id, sync_control, client):
        self.query_config_id = query_config_id
        self.sync_control = sync_control
        self.client = client
        self.obj = None

    def _get_legacy_key(self):
        return self.obj.get("legacy_key")

    def _get_name(self):
        return self.obj.get("name")

    def integrates_object(self):
        sgp_key = self.send_request_to_sgp(request_type='POST', data=self.obj)
        syncdata = models.SyncData(
            legacy_key=self._get_legacy_key(), sgp_key=sgp_key, name=self._get_name(),
            query_config_id=self.query_config_id, is_data_active=constants.EnumStatus.active, client=self.client
        )

        LOG.info(
            msg='O de chave igual a {} foi sincronizado'.format(self._get_legacy_key()),
            extra={'client': self.client, 'obj_name': self._get_name(), 'query_config_id': self.query_config_id,
                   'sgp_key': sgp_key}
        )

        return sgp_key, syncdata

    def update_object(self, sgp_key, syncdata):
        query_config = models.QueryConfig.objects.get(pk=self.query_config_id, client=self.client)

        if query_config.update_data or query_config.update_status:

            data = {}

            if query_config.update_data:
                data = self.obj
                # Recurso necessário porque o parents não é possível ser atualizado
                data.pop('parents', None)

            if query_config.update_status and not syncdata.is_data_active and not data.get('status'):
                data['status'] = constants.EnumSgpStatus.active
                syncdata.activate_sync_data()

            sgp_key = self.send_request_to_sgp(request_type='PATCH', data=data, sgp_key=sgp_key)

            LOG.info(
                msg='O dado de chave igual a {} foi atualizado'.format(self._get_legacy_key()),
                extra={'client': self.client, 'obj_name': self._get_name(), 'query_config_id': self.query_config_id,
                       'sgp_key': sgp_key}
            )

        return sgp_key, syncdata

    def get_sync_data_if_exists(self):
        sync_data = models.SyncData.objects.filter(
            legacy_key=self._get_legacy_key(),
            query_config_id=self.query_config_id,
            client=self.client
        ).first()
        return sync_data

    def send_request_to_sgp(self, *args, **kwargs):
        request_type = kwargs['request_type']

        headers = _get_request_header(self.client)
        data = json.dumps(kwargs['data'])
        url = self.get_url(**kwargs)

        for i in range(3):
            try:
                response = self.execute_request(request_type, url, headers, data)
            except Exception:
                LOG.warning(
                    msg='Falha na tentativa nº {} de envio da requisição do tipo {} '
                        'para a url {}'.format(i, request_type, url), exc_info=True,
                )
                continue
            break
        else:
            message = 'Após 3 tentativas não foi possível conectar ao serviço'
            LOG.error(
                msg='Falha ao enviar requisição para a url {}'.format(url),
                extra={'detail': message, 'client_slug': self.client, 'request_type': request_type, 'data': data}
            )
            raise Exception(message)

        status_code = response.status_code
        if self._can_register_on_sync_data(status_code):
            sgp_key = self._get_sgp_key(response)
            LOG.info(
                msg='O dado de chave igual a {} foi sincronizado'.format(self._get_legacy_key()),
                extra={'client': self.client, 'obj_name': self._get_name(), 'query_config_id': self.query_config_id,
                       'sgp_key': sgp_key}
            )
            return sgp_key

        else:
            LOG.error(
                msg='Falha na resposta da requisição ao serviço',
                extra={'request_type': request_type, 'url': url, 'response_status': response.status_code,
                       'response_reason': response.reason, 'response_text': response.text}
            )
            message = '{} {} - {}'.format(status_code, response.reason, response.text)
            raise Exception(message)

    def _can_register_on_sync_data(self, status_code):
        return status_code in [200, 201] or self._is_reuse_key_if_duplicaded(status_code)

    def _is_reuse_key_if_duplicaded(self, status_code):
        query_config = models.QueryConfig.objects.get(pk=self.query_config_id, client=self.client)
        return query_config.reuse_key_if_duplicated and status_code == 409

    def execute_request(self, request_type, url, headers, data):
        if request_type == 'POST':
            return requests.post(url, headers=headers, data=data, timeout=300)
        elif request_type == 'PATCH':
            return requests.patch(url, headers=headers, data=data, timeout=300)

    def _get_sgp_key(self, response):
        try:
            response_json = response.json()
            return self._get_key(response_json, response.text)

        except Exception as e:
            message = 'Erro ao processar response: {}'.format(str(e))
            LOG.error(
                msg=message, exc_info=True,
                extra={'response_json': response.json()}
            )
            raise Exception(message)

    def _get_key(self, response_json, response_text):
        sgp_key = self._get_key_identified_in_response(response_json)

        if sgp_key is not None:
            return sgp_key

        if "fake_sgp_key" in self.obj:
            return self.obj.get("fake_sgp_key")

        raise Exception("Chave não foi encontrada no dado retornado: " + response_text)

    def _get_key_identified_in_response(self, response_json):
        list_of_possible_key_identifiers_in_response = ["id", "key"]

        for each_key in list_of_possible_key_identifiers_in_response:
            if response_json.get(each_key, ''):
                return response_json[each_key]

    def get_url(self, *args, **kwargs):
        query_config = models.QueryConfig.objects.get(pk=self.query_config_id, client=self.client)
        url = query_config.service_url

        tradutor = TextTradutor()
        url = tradutor.convert(url, self.obj)

        request_type = kwargs['request_type']

        if request_type == 'PATCH':

            url += kwargs['sgp_key'] + '/'

        return url


class ReceiveFromSGP(object):

    def __init__(self, query_config, client, report, sync_log):
        self.query_config = query_config
        self.client = client
        self.report = report
        self.sync_log = sync_log

    def get_url(self):
        return self.query_config.service_url

    def execute(self):
        from core.services import MessagesBuilder

        connection = connectors.ConnectorODBC(self.client)
        sgp_data = self._get_from_sgp()
        for index, data in enumerate(sgp_data, start=1):
            try:
                variable_list = self._get_variables(self.query_config.query)
                query_to_execute = self._process_variables(self.query_config.query, variable_list, data)
                messages_builder = MessagesBuilder(query_to_execute)
                query_to_execute = messages_builder.build_message_with_routine(data)
                print("{} - {}<br/>".format(index, query_to_execute))
                connection.execute_query(query_to_execute)
                connection.commit()
            except NotSyncedForTransException as nse:
                message = str(nse)
                LOG.warning(
                    msg=message,
                    extra={'connection': connection, 'data': sgp_data}
                )
                self.report.report('ERROR: {}'.format(message))
                self.sync_log.register_error(self.query_config, message=message)
                continue

            except Exception as e:
                LOG.error(
                    msg='Falha na recuperação do dado {} do SGP'.format(data), exc_info=True,
                    extra={'connection': connection, 'data': sgp_data}
                )
                raise e

        connection.commit()
        LOG.info(
            msg='Sucesso na execução da query {}'.format(self.query_config.id),
            extra={'client': self.client, 'query_config': self.query_config}
        )

    def _get_from_sgp(self):
        url = self.get_url()
        headers = _get_request_header(self.client)
        for i in range(3):
            try:
                response = requests.get(url, headers=headers)
            except Exception:
                LOG.warning(
                    msg='Falha na tentativa nº {} de enviar requisição para a url {}'.format(i, url), exc_info=True
                )
                continue
            break
        else:
            message = 'Após 3 tentativas não foi possível conectar ao serviço'
            LOG.error(
                msg='Falha ao enviar requisição para a url {}'.format(url),
                extra={'detail': message, 'headers': headers}
            )
            raise Exception(message)

        response_json = response.json()
        LOG.info(
            msg='Sucesso na requisição a url {}'.format(url),
            extra={'client': self.client, 'response_status': response.status_code, 'response_reason': response.reason,
                   'response_text': response.text}
        )
        return response_json

    @staticmethod
    def _get_variables(text):
        params = re.findall(r"(?<=\$\{).+?(?=\})", text)

        return params

    def _process_variables(self, query, variable_list, sgp_data):
        attribute_processor = AttributeProcessor()
        attribute_processor.TranslateType = TranslateType.prova_facil_key_to_client_legacy_key
        for variable in variable_list:
            variable_pure = variable.split(':')[0]
            value = sgp_data.get(variable_pure)
            fake = {}
            attribute_processor.process_attribute(fake, variable, value)
            query = query.replace("${" + variable + "}", str(fake.get(variable_pure)))

        return query

    def _replace_query_plaholder(self, data):
        query = self.query_config.query
        params = re.findall(r"(?<=\$\{).+?(?=\})", query)
        for param in params:
            value = data.get(param)
            if value is None:
                value = 'Null'
            else:
                value = str(value)
            query = query.replace('${'+param+'}', value)

        return query


def _get_request_header(client):
    headers = {
        'Authorization': 'Token ' + client.token,
        'Content-type': 'application/json'
    }
    return headers
