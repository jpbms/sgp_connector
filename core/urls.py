from django.urls import path
from . import views


urlpatterns = [
    path(r'', views.dashboard_index, name='dashbord_index'),

    path(r'execute_sync_data/<int:query_config_id>', views.execute_sync_data, name='execute_sync_data'),
    path(r'get_sync_data_status/<int:limit>', views.get_sync_data_status, name='get_sync_data_status'),

    path(r'sync_all', views.sync_all, name='sync_all'),
    path(r'execute_sync_data/webconfig/<int:pk>', views.SyncDataWebConfig.as_view(),
         name='execute_sync_data_web_config'),

    path(r'generate-token', views.token_generate, name='generate_user_token'),
    path(r'token_validade', views.token_validade, name='token_validade'),
]